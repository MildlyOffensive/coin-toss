import random

# Quick program that prints the number of heads or tails coin tosses depending on what you want.
# Can print each throw as well.

# How many coin tosses you want to make
tosses = 50000
print_each_throw = True

heads = 0
tails = 0


def toss_coin():
    result = random.randint(0, 1)
    if result == 0:
        return "heads"
    else:
        return "tails"


if tosses == 0:
    print("No coins tossed.")
else:
    while tosses > 0:
        result = toss_coin()

        # increment the heads or tails depending on what came up.
        if result == "heads":
            heads += 1
        elif result == "tails":
            tails += 1
        if print_each_throw:
            print(result)
        tosses -= 1

print("\nHeads: " + str(heads))
print("Tails: " + str(tails))


